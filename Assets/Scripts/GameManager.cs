﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject gameOverCanevas;

    public GameObject check;

    // Game Instance Singleton

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        if (Options.back == "backnight")
        {
            check = GameObject.FindGameObjectWithTag("check1");
            check.transform.position = new Vector3(1f, 1.9f, 0f);
        }
        if (Options.back == "backday")
        {
            check = GameObject.FindGameObjectWithTag("check1");
            check.transform.position = new Vector3(-0.9f,1.9f, 0f);
        }
        if (Options.bird == "green")
        {
            check = GameObject.FindGameObjectWithTag("check2");
            check.transform.position = new Vector3(-1.4f, -0.4f, 0f);
        }
        if (Options.bird == "blue")
        {
            check = GameObject.FindGameObjectWithTag("check2");
            check.transform.position = new Vector3(0f, -0.4f, 0f);
        }
        if (Options.bird == "purple")
        {
            check = GameObject.FindGameObjectWithTag("check2");
            check.transform.position = new Vector3(1.4f, -0.4f, 0f);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void replay()
    {
        if (gameOverCanevas.GetComponent<Canvas>().sortingOrder != 0)
        {
            Options.score = 0;
            SceneManager.LoadScene(1);
        }
    }
    public void menu()
    {
        if (gameOverCanevas.GetComponent<Canvas>().sortingOrder != 0)
        {
            Options.score = 0;
            SceneManager.LoadScene(0);
        }
    }
    public void options()
    {
        if (gameOverCanevas.GetComponent<Canvas>().sortingOrder != 0)
        {
            Options.score = 0;
            SceneManager.LoadScene(3);
        }
    }

    public void moon()
    {
        Options.back = "backnight";
        Debug.Log(Options.back);
        check = GameObject.FindGameObjectWithTag("check1");
        check.transform.position = new Vector3(1f, 1.9f, 0f);

    }
    public void sun()
    {
        Options.back = "backday";
        Debug.Log(Options.back);
        check = GameObject.FindGameObjectWithTag("check1");
        check.transform.position = new Vector3(-0.9f,1.9f, 0f);
    }
    public void green()
    {
        Options.bird = "green";
        Debug.Log(Options.bird);
        check = GameObject.FindGameObjectWithTag("check2");
        check.transform.position = new Vector3(-1.4f, -0.4f, 0f);
    }
    public void purple()
    {
        Options.bird = "purple";
        Debug.Log(Options.bird);
        check = GameObject.FindGameObjectWithTag("check2");
        check.transform.position = new Vector3(1.4f, -0.4f, 0f);
    }
    public void blue()
    {
        Options.bird = "blue";
        Debug.Log(Options.bird);
        check = GameObject.FindGameObjectWithTag("check2");
        check.transform.position = new Vector3(0f, -0.4f, 0f);
    }
}
