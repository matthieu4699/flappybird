﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBK : MonoBehaviour
{
    Vector3 leftBottomCameraBorder;
    Vector3 movement;

    public float speed;

    Vector3 siz;

    public float PosRestart;
    // Start is called before the first frame update
    void Start()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        movement = new Vector3(-speed,0f,0f);
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = movement;												
        siz.x =	gameObject.GetComponent<SpriteRenderer>().bounds.size.x;	
		siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        if(transform.position.x	< leftBottomCameraBorder.x - (siz.x / 2))	
        {	
            transform.position = new Vector3(PosRestart,transform.position.y,transform.position.z);	
        }		
    }
}
