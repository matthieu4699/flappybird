﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class bird : MonoBehaviour
{

    public Rigidbody2D rb;
    public float moveSpeed;
    public float flapHeight;
    private Vector3 coinHautDroite;
    public GameManager gameManager;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
         coinHautDroite = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }

    void Flap()
    {
        if (rb.velocity.y < 0)
            rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(new Vector2(0, 5), ForceMode2D.Impulse);
        transform.rotation = Quaternion.Euler(0, 0, 40);
    }

    void Update()
    {
        if (gameObject.transform.position.y > coinHautDroite.y){
            SceneManager.LoadScene(2);
        }
        if (rb.velocity.x < 0){
            rb.velocity = new Vector2(0, rb.velocity.y);
            rb.rotation = 0;
        }
        if (Input.GetMouseButtonDown(0))
            Flap();
        float theta = Mathf.LerpAngle(-30, 50, Mathf.Clamp(rb.velocity.y, -1, 1));
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, theta), Time.deltaTime * 5);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "upPipe" || collision.name == "downPipe")
        {
            if(Options.score > PlayerPrefs.GetInt("highScore", 0)){
                Options.highscore = Options.score;
                PlayerPrefs.SetInt("highScore", Options.score);
            }
            SceneManager.LoadScene(2);
        }
        else if (collision.name == "point")
        {
            Options.score += 1;
        }
    }
    
}