﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tableauScore : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    { 
        Vector3	tmpPos1 = new Vector3 (-1.29f, 0.56f, 0f);
        GameObject.FindGameObjectWithTag("ScoreLabel").GetComponent<Text>().text = Options.score.ToString();
        GameObject.FindGameObjectWithTag("highScore").GetComponent<Text>().text = Options.highscore.ToString();
        if(Options.score > 29){
            GameObject b = Instantiate(Resources.Load("gold"), tmpPos1, Quaternion.identity)	as	GameObject;
        }else if(Options.score > 19){
            GameObject b = Instantiate(Resources.Load("silver"), tmpPos1, Quaternion.identity)	as	GameObject;
        }else if(Options.score > 9){
            GameObject b = Instantiate(Resources.Load("bronze"), tmpPos1, Quaternion.identity)	as	GameObject;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
