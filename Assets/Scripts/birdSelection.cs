﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class birdSelection : MonoBehaviour
{
    
    public GameObject green;
    public GameObject blue;
    public GameObject purple;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(Options.bird);
        if(Options.bird == "blue"){
            GameObject bird = Instantiate(blue);
        } else if(Options.bird == "purple"){
            GameObject bird = Instantiate(purple);
        } else{
            GameObject bird = Instantiate(green);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
